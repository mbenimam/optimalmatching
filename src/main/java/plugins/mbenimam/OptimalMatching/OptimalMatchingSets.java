package plugins.mbenimam.OptimalMatching;

import icy.file.Loader;
import icy.gui.viewer.Viewer;
import icy.main.Icy;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROIUtil;
import icy.sequence.Sequence;
import javax.swing.SwingUtilities;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.roi2d.ROI2DPath;
import plugins.kernel.roi.roi2d.ROI2DPoint;
import plugins.kernel.roi.roi2d.ROI2DPolygon;
import plugins.kernel.roi.roi2d.ROI2DRectangle;

public class OptimalMatchingSets extends EzPlug implements Block, EzStoppable {

  private boolean stopFlag;

  // final Sequence inputSequence;
  private final VarROIArray varSetA = new VarROIArray("Set A");
  private final VarROIArray varSetB = new VarROIArray("Set B");
  private VarROIArray varSetMatched = new VarROIArray("Set Matched");
  private VarROIArray varSetNotMatched = new VarROIArray("Set Not Matched");

  @Override
  public void declareInput(VarList inpuList) {
    inpuList.add(varSetA.getName(), varSetA);
    inpuList.add(varSetB.getName(), varSetB);
  }

  @Override
  public void declareOutput(VarList outputList) {
    outputList.add(varSetMatched.getName(), varSetMatched);
    outputList.add(varSetNotMatched.getName(), varSetNotMatched);
  }

  @Override
  public void stopExecution() {
    stopFlag = true;
  }

  @Override
  public void run() {}

  public static void main(String[] args) {
    // Launch the application.
    Icy.main(args);

    /*
     * Programmatically launch a plugin, as if the user had clicked its
     * button.
     */

    // Load an image.
    final String imagePath = "test.png";
    final Sequence sequence = Loader.loadSequence(imagePath, 0, true);

    // Display the images.
    try {
      SwingUtilities.invokeAndWait(() -> {
        new Viewer(sequence);
      });
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @Override
  public void clean() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Unimplemented method 'clean'");
  }

  @Override
  protected void execute() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Unimplemented method 'execute'");
  }

  @Override
  protected void initialize() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException(
      "Unimplemented method 'initialize'"
    );
  }
}
